<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $question_id
 * @property string|null $display_text
 * @property string|null $regex
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereDisplayText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereRegex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereUpdatedAt($value)
 */
class QuestionOption extends Model
{
    protected $table = 'question_options';
}