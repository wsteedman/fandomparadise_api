<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $title
 * @property string|null $slug
 * @property int|null $brand_id
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fandom whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fandom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fandom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fandom whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fandom whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fandom whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fandom whereUpdatedAt($value)
 */
class Fandom extends Model
{
    protected $table = 'fandoms';
}