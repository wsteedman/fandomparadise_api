<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $profile_id
 * @property string|null $key
 * @property string|null $value
 * @property int|null $session_id
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereProfileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfileValue whereValue($value)
 */
class ProfileValue extends Model
{
    protected $table = 'profile_values';
}