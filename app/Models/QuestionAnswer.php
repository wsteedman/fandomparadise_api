<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $question_id
 * @property int|null $profile_id
 * @property string|null $given_answer
 * @property string|null $validated_answer
 * @property int|null $is_correct
 * @property int|null $session_id
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereGivenAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereProfileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionAnswer whereValidatedAnswer($value)
 */
class QuestionAnswer extends Model
{
    protected $table = 'question_answers';
}