<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $unique_identifier
 * @property string|null $channel
 * @property int|null $session_id
 * @property string|null $meta_data
 * @property int|null $is_registered
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereIsRegistered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereMetaData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereUniqueIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profile whereUpdatedAt($value)
 */
class Profile extends Model
{
    protected $table = 'profiles';
}