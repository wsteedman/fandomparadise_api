<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property string $session_identifier
 * @property string|null $channel
 * @property int|null $profile_id
 * @property string|null $meta_data
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereMetaData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereProfileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereSessionIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereUpdatedAt($value)
 */
class Session extends Model
{
    protected $table = 'sessions';
}