<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $slug
 * @property string|null $question
 * @property string|null $question_type
 * @property string|null $image_url
 * @property int|null $fandom_id
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereFandomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuestionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereUpdatedAt($value)
 */
class Question extends Model
{
    protected $table = 'questions';
}