<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $session_id
 * @property string|null $page_name
 * @property string|null $meta_data
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hit whereMetaData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hit wherePageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hit whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hit whereUpdatedAt($value)
 */
class Hit extends Model
{
    protected $table = 'hits';
}