<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSessionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sessions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('session_identifier', 64)->index('idx_sessions_identifier');
			$table->enum('channel', array('responsive','android','ios','fb'))->nullable()->index('idx_sessions_channel');
			$table->integer('profile_id')->unsigned()->nullable()->index('fk_sessions_profile');
			$table->text('meta_data', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sessions');
	}

}
