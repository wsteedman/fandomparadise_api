<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfileValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profile_values', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('profile_id')->unsigned()->nullable()->index('fk_profval_profile');
			$table->string('key', 32)->nullable()->index('idx_profval_key');
			$table->string('value', 64)->nullable()->index('idx_profval_value');
			$table->integer('session_id')->unsigned()->nullable()->index('fk_profval_session');
			$table->boolean('status')->nullable()->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profile_values');
	}

}
