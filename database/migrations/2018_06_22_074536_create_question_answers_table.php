<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('question_answers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('question_id')->unsigned()->nullable()->index('fk_quesans_question');
			$table->integer('profile_id')->unsigned()->nullable()->index('fk_quesans_profile');
			$table->string('given_answer', 128)->nullable();
			$table->string('validated_answer', 128)->nullable()->index('idx_quesans_validated');
			$table->boolean('is_correct')->nullable()->default(1);
			$table->integer('session_id')->unsigned()->nullable()->index('fk_quesans_session');
			$table->boolean('status')->nullable()->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('question_answers');
	}

}
