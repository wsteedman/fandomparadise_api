<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('question_category', function(Blueprint $table)
		{
			$table->integer('question_id')->unsigned();
			$table->integer('category_id')->unsigned()->index('fk_quescat_category');
			$table->timestamps();
			$table->primary(['question_id','category_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('question_category');
	}

}
