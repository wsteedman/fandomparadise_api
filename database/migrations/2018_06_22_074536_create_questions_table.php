<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug', 32)->nullable();
			$table->text('question', 65535)->nullable();
			$table->enum('question_type', array('text','select'))->nullable()->index('idx_questions_questiontype');
			$table->string('image_url', 190)->nullable();
			$table->integer('fandom_id')->unsigned()->nullable()->index('fk_questions_fandom');
			$table->boolean('status')->nullable()->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questions');
	}

}
