<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProfileCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_category', function(Blueprint $table)
		{
			$table->foreign('category_id', 'fk_profcat_category')->references('id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('profile_id', 'fk_profcat_profile')->references('id')->on('profiles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_category', function(Blueprint $table)
		{
			$table->dropForeign('fk_profcat_category');
			$table->dropForeign('fk_profcat_profile');
		});
	}

}
