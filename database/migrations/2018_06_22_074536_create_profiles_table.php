<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('unique_identifier', 64)->nullable()->index('idx_profiles_identifier');
			$table->enum('channel', array('responsive','android','ios','fb'))->nullable()->index('idx_profiles_channel');
			$table->integer('session_id')->unsigned()->nullable()->index('fk_profiles_session');
			$table->text('meta_data', 65535)->nullable();
			$table->boolean('is_registered')->nullable()->default(0);
			$table->boolean('status')->nullable()->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
