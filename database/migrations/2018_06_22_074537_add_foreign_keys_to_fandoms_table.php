<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFandomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fandoms', function(Blueprint $table)
		{
			$table->foreign('brand_id', 'fk_fandoms_brand')->references('id')->on('brands')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fandoms', function(Blueprint $table)
		{
			$table->dropForeign('fk_fandoms_brand');
		});
	}

}
