<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToQuestionAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('question_answers', function(Blueprint $table)
		{
			$table->foreign('profile_id', 'fk_quesans_profile')->references('id')->on('profiles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('question_id', 'fk_quesans_question')->references('id')->on('questions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('session_id', 'fk_quesans_session')->references('id')->on('sessions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('question_answers', function(Blueprint $table)
		{
			$table->dropForeign('fk_quesans_profile');
			$table->dropForeign('fk_quesans_question');
			$table->dropForeign('fk_quesans_session');
		});
	}

}
