<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hits', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('session_id')->unsigned()->nullable()->index('fk_hits_session');
			$table->string('page_name', 32)->nullable()->index('idx_hits_pagename');
			$table->text('meta_data', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hits');
	}

}
