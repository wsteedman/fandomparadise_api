<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToQuestionCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('question_category', function(Blueprint $table)
		{
			$table->foreign('category_id', 'fk_quescat_category')->references('id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('question_id', 'fk_quescat_question')->references('id')->on('questions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('question_category', function(Blueprint $table)
		{
			$table->dropForeign('fk_quescat_category');
			$table->dropForeign('fk_quescat_question');
		});
	}

}
