<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfileCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profile_category', function(Blueprint $table)
		{
			$table->integer('profile_id')->unsigned();
			$table->integer('category_id')->unsigned()->index('fk_profcat_category');
			$table->timestamps();
			$table->primary(['profile_id','category_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profile_category');
	}

}
