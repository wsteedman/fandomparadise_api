<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFandomCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fandom_category', function(Blueprint $table)
		{
			$table->integer('fandom_id')->unsigned();
			$table->integer('category_id')->unsigned()->index('fk_fancat_category');
			$table->timestamps();
			$table->primary(['fandom_id','category_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fandom_category');
	}

}
