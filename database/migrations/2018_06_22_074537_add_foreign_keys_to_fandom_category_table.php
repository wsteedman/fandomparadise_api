<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFandomCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fandom_category', function(Blueprint $table)
		{
			$table->foreign('category_id', 'fk_fancat_category')->references('id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fandom_id', 'fk_fancat_fandom')->references('id')->on('fandoms')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fandom_category', function(Blueprint $table)
		{
			$table->dropForeign('fk_fancat_category');
			$table->dropForeign('fk_fancat_fandom');
		});
	}

}
